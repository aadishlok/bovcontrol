#Site (pt-br)
##Descrição
Crie um site responsivo que mostre uma listagem das temporadas de uma série ou show da sua escolha ao clicar. Siga o mockup abaixo para fazer algo parecido para fazer a versão mobile do site.

Mockups

https://ibb.co/mwa52G

https://ibb.co/bR8Q2G

Para isso você deverá utilizar a API do Trakt (você deve se registrar no site para ter acesso à API).https://trakt.docs.apiary.io/#reference/seasons


##Requisitos

É permitido o uso de bibliotecas externas

Tratamento de cenários de erro (erros do servidor)

Feedbacks de carregamento

Testes automatizados

Cache de imagens e da API 

##Submissão
O candidato deverá implementar a solução e enviar um pull request para este repositório com a solução.

O processo de Pull Request funciona da seguinte maneira:

1.Candidato fará um fork desse repositório (não irá clonar direto!)

2.Fará seu projeto nesse fork.

3.Commitará e subirá as alterações para o SEU fork.

4.Pela interface do Bitbucket, irá enviar um Pull Request.

5.Se possível deixar o fork público para facilitar a inspeção do código




##Site (en)
##Description
Create a responsive site that shows a listing of the seasons of a show or show of your choice upon clicking. Follow the mockup below to do something similar to make the mobile version of the site.
Mockups

https://ibb.co/mwa52G

https://ibb.co/bR8Q2G

In order to create this app you will use the Trakt API (you must sign up in the website to have access to the API).

##Requirements


It’s allowed to use external libraries

Handle error scenarios (server errors)

Loading feedback

Automated tests

Cache of images and API


###Submission
The candidate must implement the solution and send a pull request to this repository with the solution.

The Pull Request process works as follows:

1.Candidate will fork this repository (will not clone directly!)

2.It will make your project on that fork.

3.Commit and upload changes to your fork.

4.Through the Bitbucket interface, you will send a Pull Request.

5.If possible to leave the public fork to facilitate inspection of the code
